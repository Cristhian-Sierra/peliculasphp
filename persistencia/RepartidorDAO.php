<?php

class RepartidorDAO {
    private $idRepartidor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    
    function RepartidorDAO($idRepartidor = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "",$estado=""){
        $this->idRepartidor = $idRepartidor;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->correo = $correo;
        $this->clave = $clave;
        $this->foto = $foto;
        $this->estado=$estado;
    }
    
    function buscarRepartidor($filtro){
        return "select id, nombre, apellido, correo, foto
                from repartidor
                where  nombre like '%" . $filtro . "%' or apellido like '%" . $filtro . "%'";
    }
    
    function autenticar(){
        return "select id,estado from repartidor
                where correo = '" . $this -> correo . "' and clave = md5('" . $this -> clave . "')";
    }
    
    function registrar(){
        return "insert into repartidor
                (nombre, apellido, correo, clave, foto,estado)
                values ('" . $this->nombre . "', '" . $this->apellido . "', '" . $this->correo . "', md5('" . $this->clave . "'), '" . $this->foto . "','1')";
    }
    
    function consultar() {
        return "select nombre, apellido, correo,  foto
                from  repartidor
                where id =" . $this -> idRepartidor;
    }
    
    function existeCorreo(){
        return "SELECT id
                FROM repartidor
                where correo =' " . $this->correo ."'" ;
    }
    
    function consultarTodos(){
        return "select id,nombre, apellido, correo, foto,estado
                from repartidor";

    }
    public function editar(){
    	return "update repartidor
    	set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "'" . (($this -> foto!="")?", foto = '" . $this -> foto . "'":"") . "
    	where id = '" . $this -> idRepartidor .  "'";
    }
    public function cambiarEstado(){
    	return "update repartidor
    	set estado = '" . $this -> estado . "'
    	where id = '" . $this -> idRepartidor .  "'";
    }
    
}