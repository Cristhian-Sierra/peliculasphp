<?php
class ClienteDAO{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function ClienteDAO($idCliente = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idCliente = $idCliente;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from cliente
                where correo = '" . $this -> correo .  "'";
    }
        
    public function registrar(){
        return "insert into cliente (nombre,apellido,correo, clave, foto, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido . "','" . $this -> correo . "', '" . md5($this -> clave) . "' ,'" . $this -> foto . "', '1')   ";
    }
 

    
    
    public function autenticar(){
        return "select id, estado
                from cliente
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto
                from cliente
                where id = '" . $this -> idCliente .  "'";
    }
    
    public function consultarTodos(){
        return "select id, nombre, apellido, correo, foto, estado
                from cliente";
    }
    
    public function cambiarEstado(){
        return "update cliente
                set estado = '" . $this -> estado . "'
                where id = '" . $this -> idCliente .  "'";
    }
    
    public function editar(){
    	return "update cliente
    	set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', correo = '" . $this -> correo . "'" . (($this -> foto!="")?", foto = '" . $this -> foto . "'":"") . "
    	where id = '" . $this -> idCliente .  "'";
    }
    
}

?>