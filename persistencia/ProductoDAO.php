<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $imagen;
       
    public function ProductoDAO($idProducto = "", $nombre = "", $cantidad = "", $precio = "", $imagen = ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> imagen = $imagen;
    }

    public function consultar(){
        return "select nombre, cantidad, precio, imagen
                from producto
                where id = '" . $this -> idProducto .  "'";
    }    
    
    public function insertar(){
        return "insert into producto (nombre, cantidad, precio,imagen)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio . "', '" . $this -> imagen . "')";
    }
    
    public function consultarTodos(){
        return "select id, nombre, cantidad, precio,  imagen
                from producto";
    }
    


    public function consultarCantidad(){
        return "select count(id)
                from producto";
    }
 
    public function editar(){
        return "update producto
                set nombre = '" . $this -> nombre . "', cantidad = '" . $this -> cantidad . "', precio = '" . $this -> precio . "'" . (($this -> imagen!="")?", imagen = '" . $this -> imagen . "'":"") . "
                where id = '" . $this -> idProducto .  "'";
    }
  
    public function consultarFiltro($filtro){
        return "select id, nombre, cantidad, precio, imagen
                from producto
                where nombre like '%" . $filtro . "%' or cantidad like '" . $filtro . "%' or precio like '" . $filtro . "%'";
    }
    
}

?>