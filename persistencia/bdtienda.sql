	create database tiendal;
	use tiendal;
	
	create table cliente( 
	id integer auto_increment primary key not null,
	nombre varchar(45),
	apellido varchar(45),
	correo varchar(45),
	clave varchar(45),
	foto varchar(45),
	estado int
	
	);
	create table administrador(
	id integer auto_increment primary key not null,
	nombre varchar(45),
	apellido varchar(45),
	correo varchar(45),
	clave varchar(45),
	foto varchar(45));
	
	
	create table producto(
	id integer auto_increment primary key not null,
	nombre varchar(45),
	cantidad int,
	precio double,
	imagen varchar(45)
	);
	
	create table repartidor(
	id integer auto_increment primary key not null,
	nombre varchar(45),
	apellido varchar(45),
	correo varchar(45),
	clave varchar(45),
	foto varchar(45)
	);
	
	
	

	
	create table factura(
	id integer auto_increment primary key not null,
	fecha date,
	valor double,
	id_cliente integer,
	foreign key(id_cliente) references cliente(id)
	);
    
    create table pedido(
	id integer auto_increment primary key not null,
	id_repartidor integer not null,
	id_factura integer not null,
	estado varchar(25),
	foreign key (id_repartidor) references repartidor(id),
	foreign key (id_factura) references factura (id)
	);
	create table factuaproducto(
	id_pedido integer,
	id_producto integer,
	cantidad int,
	precio double,
	foreign key(id_pedido) references pedido(id),
	foreign key(id_producto) references producto(id),
	primary key (id_pedido,id_producto)
	
	);
	
	
	alter table repartidor add column estado integer;
	
