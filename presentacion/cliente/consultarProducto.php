<?php
$producto = new Producto();
$productos = $producto -> consultarTodos();
if (isset($_POST["RegistroPedido"])){
	$pedido = new Pedido("","",$_SESSION['Reserva']);
	$pedido -> registrar();
	$pedido -> consultarId();
	$p = $pedido ->getIdPedido();
	foreach ($_SESSION['cesta'] as $r) {
		$producto=$r['idProducto'];
		$cantidad=$r['cantidad'];
	
		$pe_pl = new Factura_Producto($p, $producto,  $cantidad);
		$pe_pl -> registrar();
	}
	$reserva = new Factura($_SESSION['Reserva'], "", "", "");
	$reserva -> actualizarEstado();
	$_SESSION['cesta']=array();
	$_SESSION['contador']=0;
}

?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Consultar Producto</h4>
				</div>
				<div class="text-right"><?php echo count($productos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
	
							<th>Precio</th>
							<th>Imagen</th>
							<th>Incluir al carrito </th>
						</tr>
						<?php 
						$i=1;
						foreach($productos as $productoActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $productoActual -> getNombre() . "</td>";
						    echo "<td>" . $productoActual -> getPrecio() . "</td>";
						    echo "<td>" . (($productoActual -> getImagen()!="")?"<img src='" . $productoActual -> getImagen() . "' height='80px'>":"") . "</td>";
						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/cliente/agregarProducto.php") . "&idProducto=" . $productoActual -> getIdProducto(). "' data-toggle='tooltip' data-placement='left' title='incluir'><span class='fas fa-edit'></span></a></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>	     
		    