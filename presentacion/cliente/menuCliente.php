<?php
$cliente = new Cliente($_SESSION["id"]);
$cliente -> consultar();
$items = 1;
if($cliente -> getNombre() != ""){
    $items++;
}
if($cliente -> getApellido() != ""){
    $items++;
}
if($cliente -> getFoto() != ""){
    $items++;
}
$porcentaje = $items/4 * 100;
?>
<nav class="navbar navbar-expand-md navbar-dark bg-danger">
	<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/sesionCliente.php") ?>"><i class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false">Solicitar producto</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarProducto.php") ?>">Mirar productos</a>
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/carritoProducto.php") ?>">Ver carrito de compras</a>
				</div>
				</li>
				<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false">Editar</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/editarCliente.php") ."&idCliente=" . $cliente -> getIdCliente()?>">Modificar datos</a>
				</div>
				</li>
				
				
		</ul>
		<ul class="navbar-nav">
			<li ><a class="nav-link text-white"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false"><span class="badge badge-warning"><?php echo $porcentaje . "%"?></span>
				Cliente: <?php echo ($cliente -> getNombre()!=""?$cliente -> getNombre():$cliente -> getCorreo()) ?> <?php echo $cliente -> getApellido() ?></a>
				
			<li class="nav-item active"><a class="nav-link" href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>