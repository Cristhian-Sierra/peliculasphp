<?php
if(isset($_POST["editar"])){
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "fotos/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $cliente = new Cliente($_GET["idCliente"]);
        $cliente -> consultar();
        if($cliente -> getFoto() != ""){
            unlink($cliente -> getFoto());
        }
        $cliente = new Cliente($_GET["idCliente"], $_POST["nombre"], $_POST["apellido"], $_POST["correo"],"", $rutaRemota);
        $cliente -> editar();
    }else{
    	$cliente = new Cliente($_GET["idCliente"], $_POST["nombre"], $_POST["apellido"], $_POST["correo"],"");
    	$cliente-> editar();
    	echo("No se ha modificado foto :(");
    }
}else{
    $cliente = new Cliente($_GET["idCliente"]);
    $cliente -> consultar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Editar Cliente</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["editar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/cliente/editarCliente.php") ?>&idCliente=<?php echo $_GET["idCliente"]?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $cliente -> getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control"  value="<?php echo $cliente -> getApellido() ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="text" name="correo" class="form-control" value="<?php echo $cliente-> getCorreo() ?>" required>
						</div>
						<div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						<button type="submit" name="editar" class="btn btn-danger">Editar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>