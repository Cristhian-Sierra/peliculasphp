<?php


$idPedido = $_GET["idPedido"];
$pe = new Pedido($_GET["idPedido"] );
$pe -> consultar();
$factura = new Factura($pe->getIdFactura());
$factura-> consultar();
$cliente = new Cliente($factura ->getIdcliente());
$cliente -> consultar();
$factura_p = new Factura_Producto($idPedido);
$p = $factura_p->consultarReservaProducto();
$total=0;
$precio=0;
foreach ($p as $p_p){
    $producto = new Producto($p_p ->getIdProducto());
    $producto -> consultar();
    $precio = $p_p ->getCantidad()*$producto->getPrecio();
    $total = $total + $precio;
}
$factura = new Factura("","",$total,$idCliente);
$factura -> registrar();
$pdf =new Cezpdf('a4');

$cols = array('id' => 'ID',
    'producto' => 'Producto' ,
    'precio' => 'Precio Unidad' ,
    'cantidad' => 'Cantidad'  
);
$i=0;

foreach($p as $pa){
    $producto = new Producto($p_p ->getIdProducto());
    $producto -> consultar();
    $datos[$i]=array(
        "id" => $idPedido,
        "plato" => $producto->getNombre(),
        "precio" => $producto->getPrecio(),
        "cantidad" => $pa->getCantidad(),
    );
    $i=$i+1;
    echo($datos);
}


$pdf->ezTable($datos,$cols,"",$opciones);
$pdf->ezText("\n\n",10);
$pdf->ezText("Total a pagar:".$total,20);
$pdf->ezText("Cliente: ".$cliente->getNombre()." ".$cliente->getApellido(),15);
$pdf->ezText("\n",10);
$pdf->ezText("<b>Fecha:</b> ".date("d/m/Y"),10);
$pdf->ezText("<b>Hora:</b> ".date("H:i:s")."\n\n",10);

ob_end_clean();
$pdf->ezStream();