<?php
$cliente = new Cliente($_SESSION['id']);
$producto = new Producto($_GET["idProducto"]);
//$cliente->consultar();
//include 'presentacion/cliente/menuliente.php';
?>
<div class="container my-4">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header bg-danger text-white">Agregar Al Carrito</div>
				<div class="card-body">
						<?php if (isset($_POST["agregar"])) { ?>
						<div class="alert alert-success" role="alert">Carrito exitosamente.</div>						
						<?php } ?>
						<form action=<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/carritoProducto.php")."&idProducto=" . $_GET['idProducto'] ?>  method="post" >
						<div class="form-group">
							<input type="number" name="cantidad" class="form-control" min="1" placeholder="Cantidad" required="required"">
						</div>
						<div class="form-group">
							<label><?php echo $producto-> getPrecio();?></label>
						</div>
						<button type="submit" name="registrar" class="btn btn-danger">Agregar Al Carrito</button>
					</form>
				</div>
			</div>
		</div>

	</div>

</div>
