<div class="container mt-3">
	<div class="row">
		<div class="col-lg-9 col-md-8 col-12">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Bienvenido a la tienda de libros</h4>
				</div>
              	<div class="card-body">
              		<img src="img/portada.jpg" width="100%">
            	</div>
            </div>
		</div>
		<div class="col-lg-3 col-md-4 col-12 text-center">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Autenticacion</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>" method="post">
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="ingresar" type="submit" class="form-control btn btn-danger">
    					</div>
    					<?php 
    					if(isset($_GET["error"]) && $_GET["error"]==1){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
    					}else if(isset($_GET["error"]) && $_GET["error"]==4){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Repartidor, su cuenta ha sido inhabilitada </div>";    					   
    					}else if(isset($_GET["error"]) && $_GET["error"]==3){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
    					}
    					?>
        			</form>
        			<p>Aun no te registras? <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/registrarCliente.php")?>">Registrate</a></p>
            	</div>
            </div>
		</div>
	</div>
</div>
