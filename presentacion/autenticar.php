<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$cliente = new Cliente("", "", "", $correo, $clave);
$repartidor = new Repartidor("", "", "", $correo, $clave);

if($administrador -> autenticar()){
    $_SESSION["id"] = $administrador -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    header("Location: index.php?pid=" . base64_encode("presentacion/administrador/sesionAdministrador.php"));
}


else if($cliente -> autenticar()){
    if($cliente -> getEstado() == 0){
        header("Location: index.php?error=3");        
    }else{
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";
        $_SESSION['contador']=0;
        $_SESSION['cesta']=array();
        header("Location: index.php?pid=" . base64_encode("presentacion/cliente/sesionCliente.php"));
    }
}
else if($repartidor -> autenticar()){
	if($repartidor -> getEstado()==0){
		header("Location: index.php?error=4");
	}else if($repartidor->getEstado()==1){
		$_SESSION["id"] = $repartidor -> getIdRepartidor();
		$_SESSION["rol"] = "Repartidor";
		header("Location: index.php?pid=" . base64_encode("presentacion/repartidor/sesionRepartidor.php"));
		echo("Puede pasarr");
	}
}

else{
    header("Location: index.php?error=1");
}
?>