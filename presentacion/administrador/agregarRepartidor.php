<?php 

$error = 0;
$registrado = false;
$foto="";
if(isset($_POST["registrar"])){
	$nombre = $_POST["nombre"];
	$apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
   // $foto = $_POST["foto"];
    
    $repartidor= new Repartidor("", "$nombre", "$apellido", "$correo", "$clave", "$foto");
    if($repartidor -> existeCorreo()){
        $error = 1;
    }else if($_FILES["foto"]["name"] != ""){
		$rutaLocal = $_FILES["foto"]["tmp_name"];
		$tipo = $_FILES["foto"]["type"];
		$tiempo = new DateTime();
		$rutaRemota = "fotos/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
		copy($rutaLocal,$rutaRemota);
    	$repartidor = new Repartidor("", $nombre, $apellido, $correo,$clave, $foto);
    if($repartidor -> getFoto() != ""){
    	unlink($cliente -> getFoto());
    }
    $repartidor = new Repartidor("", $nombre, $apellido, $correo,$clave,$rutaRemota);
    $repartidor-> registrar();
    }else{
    	$repartidor = new Repartidor("", $nombre, $apellido, $correo,$clave);
    	$repartidor -> registrar();
    }
       
        $registrado = true;
    
}
?>



<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Agregar repartidor</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/administrador/agregarRepartidor.php") ?>" method="post" enctype="multipart/form-data">
        				<div class="form-group">
    						<input name="nombre" type="text" class="form-control" placeholder="Nombres" required>
    					</div>
        				<div class="form-group">
    						<input name="apellido" type="text" class="form-control" placeholder="Apellidos" required>
    					</div>
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="foto" type="file" class="form-control ">
    					</div>	
        				<div class="form-group">
    						<input name="registrar" type="submit" class="form-control btn btn-danger">
    					</div>
    						    					
        			</form>     
        			<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El correo <?php echo $correo ?> ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($registrado) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						El  usuario <?php echo $nombre,$apellido ?> ha sido registrado correctamente.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
            	</div>
            </div>		
		</div>
	</div>
</div>

