<?php
$idRepartidor= $_GET["idRepartidor"];
$nuevoEstado = $_GET["nuevoEstado"];

echo "<a id='cambiarEstado" . $idRepartidor . "' href='#' >" . (($nuevoEstado==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>") . "</a>";
?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idRepartidor ?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoRepartidorAjax.php") ?>&idRepartidor=<?php echo $idRepartidor ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";		
		$("#icono<?php echo $idRepartidor ?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoAccionAjax.php") ?>&idRepartidor=<?php echo $idRepartidor?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idRepartidor ?>").load(url);
	});		
});
</script>
