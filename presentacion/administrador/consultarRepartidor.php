<?php
$repartidor= new Repartidor();
$repartidores = $repartidor -> consultarTodos();
?>
<div class="container mt-4">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
		<input id="Filtro" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		</div>
	</div>
</div>


<div class="container mt-3">
	<div class="row">
		<div class="col">
		<div id="resultados">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Consultar Repartidor</h4>
				</div>
				<div class="text-right"><?php echo count($repartidores) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Foto </th>
							<th>Estado</th>
							<th>Servicios</th>
						</tr>
						<?php 
						$i=1;
						foreach($repartidores as $repartidorActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $repartidorActual -> getNombre() . "</td>";
						    echo "<td>" . $repartidorActual -> getApellido() . "</td>";
						    echo "<td>" . $repartidorActual -> getCorreo() . "</td>";
						    echo "<td>" . (($repartidorActual -> getFoto()!="")?"<img src='" . $repartidorActual -> getFoto() . "' height='80px'>":"") . "</td>";
		
						    echo "<td>" . (($repartidorActual -> getEstado()==1)?"<div id='icono" . $repartidorActual -> getIdRepartidor() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($repartidorActual -> getEstado()==0)?"<div id='icono" . $repartidorActual -> getIdRepartidor() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";						    
						    echo "<td><div id='accion" . $repartidorActual -> getIdRepartidor() . "'><a id='cambiarEstado" . $repartidorActual -> getIdRepartidor() . "' href='#' >" . (($repartidorActual -> getEstado()==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":(($repartidorActual -> getEstado()==0)?"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>":"")) . "</a>";
						?>  
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $repartidorActual -> getIdRepartidor() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoRepartidorAjax.php") ?>&idRepartidor=<?php echo $repartidorActual -> getIdRepartidor() ?>&nuevoEstado=<?php echo (($repartidorActual -> getEstado()==1)?"0":"1")?>";		
                        		$("#icono<?php echo $repartidorActual -> getIdRepartidor() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoAccionAjax.php") ?>&idRepartidor=<?php echo $repartidorActual -> getIdRepartidor() ?>&nuevoEstado=<?php echo (($repartidorActual -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php echo $repartidorActual -> getIdRepartidor() ?>").load(url);
                        	});
                        });
                        </script>
						<?php   						    
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#Filtro").keyup(function(){
	     var fil = $("#Filtro").val();
	     console.log(fil);
	     if(fil.length>=1){
		     <?php echo "var ruta = \"indexAjax.php?pid=". base64_encode("presentacion/administrador/consultarRepartidorAjax.php")."\";\n";?>
			 $("#resultados").load(ruta,{fil});
	     }
	
	});
});
</script>
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     
		     