<?php
$r = new Repartidor();
$filtro = $_REQUEST["fil"];
$repartidores = $r -> buscarRepartidor($filtro);
?>
<div class="card">
	<div class="card-header bg-danger text-white">Consultar Repartidor</div>
	<div class="card-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Nombre</th>
					<th scope="col">Apellido</th>
					<th scope="col">Correo</th>
					<th >Foto</th>
				</tr>
			</thead>
			<tbody>
						<?php
    foreach ($repartidores as $r) {
        echo "<tr>";
        echo "<td>" . $r->getIdRepartidor() . "</td>";
        echo "<td>" . $r->getNombre() . "</td>";
        echo "<td>" . $r->getApellido() . "</td>";
        echo "<td>" . $r->getCorreo() . "</td>";
        echo "<td>" . (($r -> getFoto()!="")?"<img src='" . $r -> getFoto() . "' height='80px'>":"") . "</td>";
        echo "<td>" . (($r -> getEstado()==1)?"<div id='icono" . $r -> getIdRepartidor() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($r -> getEstado()==0)?"<div id='icono" . $r -> getIdRepartidor() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";
        echo "<td><div id='accion" . $r -> getIdRepartidor() . "'><a id='cambiarEstado" . $r -> getIdRepartidor() . "' href='#' >" . (($r -> getEstado()==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":(($r -> getEstado()==0)?"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>":"")) . "</a>";
        
        echo "</tr>";
    }
    echo "<tr><td colspan='9'>" . count($repartidores) . " registros encontrados</td></tr>"?>	
</tbody>
		</table>
	</div>
</div>