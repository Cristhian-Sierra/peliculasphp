<?php

require 'persistencia/PedidoDAO.php';
require_once 'persistencia/Conexion.php';

class Pedido {
    private $idPedido;
    private $idFactura;
    private $idRepartidor;
    private $estado;
    private $pedidoDAO;
    private $conexion;
    
    function getIdPedido(){
        return $this -> idPedido;
    }
    
    function getIdFactura(){
        return $this -> idFactura;
    }
    
    function getIdRepartidor(){
        return $this -> idRepartidor;
    }
    
    
    function getEstado(){
        return $this -> estado;
    }
    
    
    function Pedido($idPedido="", $idRepartidor="", $idFactura="" , $estado=""){
        $this -> idpedido = $idpedido;
        $this -> idFactura = $idFactura;
        $this -> idRepartidor = $idRepartidor;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> pedidoDAO = new PedidoDAO($idPedido,$idRepartidor, $idFactura, $estado);
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pedidoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> idFactura = $resultado[0];
        $this -> estado = $resultado[1];
        $this -> conexion -> cerrar();
    }
    
    function consultarId(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pedidoDAO -> consultarId());
        $resultado = $this -> conexion -> extraer();
        $this -> idPedidoedido = $resultado[0];
        $this -> conexion -> cerrar();
    }

    function actualizarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pedidoDAO -> actualizarEstado());
        $this -> conexion -> cerrar();
    }
    
    function actualizarRepartidor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pedidoDAO -> actualizarRepartidor());
        $this -> conexion -> cerrar();
    }
    
    function buscarPedido($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pedidoDAO -> buscarPedido($filtro));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Pedido($registro[0], $registro[1], $registro[2] , $registro[3]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pedidoDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pedidoDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Pedido($registro[0], $registro[1], $registro[2], $registro[3]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function consultarPedidoRepartidor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> pedidoDAO -> consultarPedidoRepartidor());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Pedido($registro[0], $registro[1], $registro[2],"");
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
}

?>

