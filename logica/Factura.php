<?php

require 'persistencia/FacturaDAO.php';
require_once 'persistencia/Conexion.php';

class Factura {
    private $idFactura;
    private $fecha;
    private $valor;
    private $idCliente;
    private $facturaDAO;
    private $conexion;
    
    
    function getIdFactura(){
        return $this -> idreserva;
    }
    
   
    
    function getFecha(){
        return $this -> fecha;
    }
    
    function getValor(){
    	return $this -> valor;
    }
    function getIdCliente(){
        return $this -> idCliente;
    }
    
 
 
    
    
    function Reserva($idFactura="", $fecha="", $valor="",$idCliente=""){
        $this -> idFactura = $idFactura;
        $this -> fecha = $fecha;
        $this -> valor= $valor;
        $this -> idCliente = $idCliente;
       
        $this -> conexion = new Conexion();
        $this -> facturaDAO = new FacturaDAO($idFactura, $fecha,$valor, $idCliente);
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> reservaDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Reserva($registro[0], $registro[1], $registro[2],$registro[3], $registro[4], $registro[5], $registro[6]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    

    
    
    function ValidarFactura(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> ValidarFactura());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return true;
        } else {
            $this -> conexion -> cerrar();
            return false;
        }
    }
    
    function consultarFacturaCliente(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> consultarFacturaCliente());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Factura($registro[0], $registro[1], $registro[2],$registro[3]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
    function buscarFactura($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> buscarFactura($filtro));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Factura($registro[0], $registro[1], $registro[2], $registro[3]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> idFactura = $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> valor = $resultado[2];
        $this -> idCliente = $resultado[3];
      
        $this -> conexion -> cerrar();
    }
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> registrar());
        $this -> conexion -> cerrar();
        
    }
    
    
    function consultarFactura(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> consultarFactura());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i][0] = $registro[0];
            $resultados[$i][1] = $registro[1];
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    

    
}

?>
