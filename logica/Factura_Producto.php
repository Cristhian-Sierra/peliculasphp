<?php
require 'persistencia/Factura_ProductoDAO.php';
require_once 'persistencia/Conexion.php';
class Factura_Producto{
	private $id_pedido;
    private $id_producto;
    private $cantidad;
    private $precio;
    private $Factura_ProductoDAO;
    private $conexion;
    
    function getIdPedido(){
        return $this -> id_pedido;
    }
    
    function getIdProducto(){
        return $this -> id_producto;
    }
    
    function getPrecio(){
    	return $this -> precio;
    } 
    
    function getCantidad(){
        return $this -> cantidad;
    }
    
 
       
    function Factura_Producto($id_pedido="", $id_producto="", $cantidad = "",$precio=""){
        $this -> id_pedido = $id_pedido;
        $this -> id_producto = $id_producto;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> conexion = new Conexion();
        $this -> Factura_ProductoDAO = new Factura_ProductoDAO( $id_pedido, $id_producto, $cantidad);
    }
    
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> Factura_ProductoDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    function consultarReservaProducto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> Factura_ProductoDAO -> consultarReservaProducto());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Factura_Producto("",$registro[0], $registro[1], $registro[2]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> Factura_ProductoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> id_pedido = $resultado[0];
        $this -> id_producto = $resultado[1];
        $this -> cantidad = $resultado[2];
        $this -> conexion -> cerrar();
    }
    
    function consultarProductoVendido(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> Factura_ProductoDAO -> consultarPlatoVendido());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i][0] = $registro[0];
            $resultados[$i][1] = $registro[1];
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
}